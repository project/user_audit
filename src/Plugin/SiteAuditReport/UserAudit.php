<?php

namespace Drupal\user_audit\Plugin\SiteAuditReport;

use Drupal\site_audit\Plugin\SiteAuditReportBase;

/**
 * Provides a User Audit Report.
 *
 * @SiteAuditReport(
 *  id = "user_audit",
 *  name = @Translation("User Settings"),
 *  description = @Translation("List of user settings.")
 * )
 */
class UserAudit extends SiteAuditReportBase {}
