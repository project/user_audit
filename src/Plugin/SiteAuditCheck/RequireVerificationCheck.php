<?php

namespace Drupal\user_audit\Plugin\SiteAuditCheck;

use Drupal\site_audit\Plugin\SiteAuditCheckBase;
use Drupal\Core\Url;

/**
 * Check to very that site requires email verification.
 *
 * @SiteAuditCheck(
 *  id = "requires_verification",
 *  name = @Translation("Email Verification Required"),
 *  description = @Translation("Check that email verification is required for account creation."),
 *  report = "user_audit"
 * )
 */
class RequireVerificationCheck extends SiteAuditCheckBase {

  /**
   * {@inheritdoc}
   */
  public function getResultFail() {}

  /**
   * {@inheritdoc}
   */
  public function getResultInfo() {}

  /**
   * {@inheritDoc}
   */
  public function getResultPass() {
    return $this->t('User email verification is enabled.');
  }

  /**
   * {@inheritDoc}
   */
  public function getResultWarn() {
    return $this->t('User email verification is not enabled.');
  }

  /**
   * {@inheritdoc}
   */
  public function getAction() {
    if ($this->score == SiteAuditCheckBase::AUDIT_CHECK_SCORE_WARN) {
      return $this->t('Go to the <a href=":manage-user">Account settings page</a> and enable "Require email verification when a visitor creates an account".', [
        ':manage-users' => Url::fromRoute('entity.user.admin_form'),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateScore() {
    $setting = \Drupal::config('user.settings')->get('verify_mail');
    if ($setting) {
      return SiteAuditCheckBase::AUDIT_CHECK_SCORE_PASS;
    }
    return SiteAuditCheckBase::AUDIT_CHECK_SCORE_FAIL;
  }

}
