<?php

namespace Drupal\user_audit\Plugin\SiteAuditCheck;

use Drupal\site_audit\Plugin\SiteAuditCheckBase;

/**
 * Check to very that site requires email verification.
 *
 * @SiteAuditCheck(
 *  id = "login_settings",
 *  name = @Translation("Login Settings"),
 *  description = @Translation("User settings pertaining to failed logins"),
 *  report = "user_audit"
 * )
 */
class LoginSettingsCheck extends SiteAuditCheckBase {

  /**
   * {@inheritdoc}
   */
  public function getResultFail() {}

  /**
   * {@inheritdoc}
   */
  public function getResultInfo() {
    $table = [
      '#header' => ['Setting', 'Value'],
      '#theme' => 'table',
    ];
    $loginAttempts = \Drupal::config('user.flood')->get('user_limit');
    $table['#rows'][] = ['Failed login attempts by user', $loginAttempts];
    $loginWindow = \Drupal::config('user.flood')->get('user_window');
    $loginWindowTime = $this->getTime($loginWindow);
    $table['#rows'][] = ['Failed login by user lock out window', $loginWindowTime];
    $ipAttempts = \Drupal::config('user.flood')->get('ip_limit');
    $table['#rows'][] = ['Failed login attempts by IP', $ipAttempts];
    $ipWindow = \Drupal::config('user.flood')->get('ip_window');
    $ipWindowTime = $this->getTime($ipWindow);
    $table['#rows'][] = ['Failed login by IP lock out window', $ipWindowTime];
    return $table;
  }

  /**
   * {@inheritDoc}
   */
  public function getResultPass() {}

  /**
   * {@inheritDoc}
   */
  public function getResultWarn() {}

  /**
   * {@inheritdoc}
   */
  public function getAction() {}

  /**
   * {@inheritdoc}
   */
  public function calculateScore() {
    return SiteAuditCheckBase::AUDIT_CHECK_SCORE_INFO;
  }

  /**
   * Converts seconds to hours, minutes and seconds.
   *
   * @param int $seconds
   *   The number of seconds to convert.
   *
   * @return string
   *   Return a formatted string.
   */
  private function getTime(int $seconds) {
    $time = floor($seconds / 3600);
    $time .= ($time == 1) ? ' hour' : ' hours';
    if ($minutes = $seconds % 3600) {
      $time .= ' ' . $minutes;
      $time .= ($minutes == 1) ? ' minute' : ' minutes';
    }
    return $time;
  }

}
