<?php

namespace Drupal\user_audit\Plugin\SiteAuditCheck;

use Drupal\site_audit\Plugin\SiteAuditCheckBase;
use Drupal\user\Entity\Role;

/**
 * Check to very that site requires email verification.
 *
 * @SiteAuditCheck(
 *  id = "privileged_users",
 *  name = @Translation("Privileged User Roles"),
 *  description = @Translation("Check which roles can edit users."),
 *  report = "user_audit"
 * )
 */
class PrivilegedUserCheck extends SiteAuditCheckBase {

  /**
   * {@inheritdoc}
   */
  public function getResultFail() {
    return $this->t("No roles can edit users!");
  }

  /**
   * {@inheritdoc}
   */
  public function getResultInfo() {
    $roles = $this->getRoles();
    $header = $this->formatPlural(count($roles), 'Role', 'Roles');
    $table = [
      '#header' => [$header],
      '#theme' => 'table',
    ];
    foreach ($roles as $label) {
      $table['#rows'][] = [$label];
    }
    return $table;
  }

  /**
   * {@inheritDoc}
   */
  public function getResultPass() {}

  /**
   * {@inheritDoc}
   */
  public function getResultWarn() {}

  /**
   * {@inheritdoc}
   */
  public function getAction() {}

  /**
   * {@inheritdoc}
   */
  public function calculateScore() {
    $roles = $this->getRoles();
    if (empty($roles)) {
      return SiteAuditCheckBase::AUDIT_CHECK_SCORE_FAIL;
    }
    else {
      return SiteAuditCheckBase::AUDIT_CHECK_SCORE_INFO;
    }
  }

  /**
   * Checks which roles can edit users.
   */
  private function getRoles() {
    $allRoles = Role::loadMultiple();
    $roles = [];
    foreach ($allRoles as $roleObj) {
      if ($roleObj->hasPermission('administer users')) {
        $roles[] = $roleObj->get('label');
      }
    }
    return $roles;
  }

}
