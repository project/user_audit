# User Audit

## Introduction

The _User Audit_ module adds checks and a report to the [Site Audit](https://www.drupal.org/project/site_audit) module. The report lists the user settings for failed login attempts, whether or not email verification is required for new users, and lists the roles that can administer users.

As well as providing the checks and report for Site Audit, this module also provides the same checks using Drush commands. `drush ct:email-verification` and  `drush ct:failed-logins` list the same information as the Site Audit check. `drush ct:privileged-users` instead of listing the roles that can administer users, lists users with the permission to administer users, or, you can pass a role(s) or permission(s) to the command, for example, `drush ct:privileged-users --roles=Administrator,Manager`, to list all users with the given role(s) or permission(s).

## Requirements

This module is designed to work with Drupal 8.4 or greater and Drush 9 or
greater.

## Installation

* Install as you would normally install a contributed Drupal module.
  Visit [Installing Drupal 8 Modules](https://www.drupal.org/node/1897420]) for further information.

## Usage

### With Site Audit

_User Audit_ adds checks and a report to the [Site Audit](https://www.drupal.org/project/site_audit) Report. Simply go to _Reports_ -> _Site Audit_ and the results will be listed in the *User Settings* section. The only difference between the Drush results and the _Site Audit_ results is that the *Privileged User* check only returns the roles with the permission to _administer users_.

### Drush

#### List Privileged Users

To list all users that can administer users run `drush ct:privileged-user`

To list all users with the _Administrator_ role run `drush ct:privileged-users --role=Administrator`

You can list multiple roles using a comma separated list, for example, `drush ct:privileged-users --role=Administrator, Manager`

You can also use the _drush_ alias, for example, run `drush ct-lpu --perms="Administer users, Change own username"` to list all users with the _Administer users_ and _Change own users_
permissions.

#### Get Failed Login attempt settings

To check the settings for failed logins and lockout window, run `drush ct:failed-logins`

#### Validate email verification required

To check whether email verification is required when a visitor creates an account, run `drush ct:email-verification`

## Maintainers

Current maintainers:

* [Tom Camp (Tom.Camp)](https://www.drupal.org/u/tomcamp)

This project has been sponsored by:

* [CivicActions, Inc.](https://www.drupal.org/civicactions)
  CivicActions empowers U.S. government agencies to deliver delightful digital
  experiences that are as innovative and rewarding as popular online and mobile
  consumer services.
